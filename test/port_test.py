from lyst.domain import UserCommand, UserAction, Project
from lyst.port import ProjectController


class TestableProjectController(ProjectController):
    def parse_command(self, project: Project, command: str) -> UserCommand:
        pass

    def read_command(self) -> UserCommand:
        pass


class ProjectControllerTest:
    def setup_method(self):
        self.controller = TestableProjectController()
        self.project = Project()
        self.project.create_theme('do this')
        self.project.current_theme.enrich('first task')
        self.project.current_theme.finish(0)

    def test_add_idea_command_creates_a_new_idea_in_current_theme(self):
        # given
        command = UserCommand(UserAction.ADD_IDEA, 'second task')

        # when
        self.controller.execute_command(self.project, command)

        # then
        assert self.project.current_theme.ideas[1].description == 'second task'

    def test_finish_idea_command_tags_an_idea_as_done(self):
        # given
        self.project.current_theme.enrich('second task')
        command = UserCommand(UserAction.FINISH_IDEA, '1')

        # when
        self.controller.execute_command(self.project, command)

        # then
        assert self.project.current_theme.ideas[1].done

    def test_switch_theme_command_changes_the_current_project_theme(self):
        # given
        self.project.create_theme('do that')
        self.project.create_theme('do nothing')
        command = UserCommand(UserAction.SWITCH_THEME, '1')

        # when
        self.controller.execute_command(self.project, command)

        # then
        assert self.project.current_theme.goal == 'do that'

    def test_create_theme_command_adds_a_theme_to_the_project(self):
        # given
        command = UserCommand(UserAction.CREATE_THEME, 'second task')

        # when
        self.controller.execute_command(self.project, command)

        # then
        assert len(self.project.themes) == 2
        assert self.project.current_theme.goal == 'second task'
